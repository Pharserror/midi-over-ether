import React from 'react';
import Loader from '@src/react/components/Loader';

describe('Loader', () => {
  it('renders', () => {
    const wrapper = shallow( <Loader /> );

    expect(wrapper).toMatchSnapshot();
  });
});
