import React from 'react';
import HelloWorld from '@src/react/components/HelloWorld';

describe('HelloWorld', () => {
  it('renders', () => {
    const wrapper = shallow( <HelloWorld /> );

    expect(wrapper).toMatchSnapshot();
  });
});
