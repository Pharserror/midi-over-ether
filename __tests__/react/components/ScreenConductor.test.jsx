import React from 'react';
import ScreenConductor from '@src/react/components/ScreenConductor';

describe('ScreenConductor', () => {
  it('renders', () => {
    const wrapper = shallow( <ScreenConductor /> );

    expect(wrapper).toMatchSnapshot();
  });
});
