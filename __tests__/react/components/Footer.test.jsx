import React from 'react';
import Footer from '@src/react/components/Footer';

describe('Footer', () => {
  it('renders', () => {
    const wrapper = shallow( <Footer /> );

    expect(wrapper).toMatchSnapshot();
  });
});
