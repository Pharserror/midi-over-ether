import React from 'react';
import SitewideNav from '@src/react/components/SitewideNav';

describe('SitewideNav', () => {
  it('renders', () => {
    const wrapper = shallow( <SitewideNav /> );

    expect(wrapper).toMatchSnapshot();
  });
});
