import React from 'react';
import Root from '@src/react/components/Root';

describe('Root', () => {
  it('renders', () => {
    const wrapper = shallow( <Root /> );

    expect(wrapper).toMatchSnapshot();
  });
});
