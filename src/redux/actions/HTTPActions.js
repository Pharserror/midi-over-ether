import ActionCreator from 'actr';

export default new ActionCreator([
  'parseDataIntoStore',
  'ajaxError'
]);
