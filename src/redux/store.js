import {
  applyMiddleware,
  combineReducers,
  compose,
  createStore
} from 'redux';

import thunk from 'redux-thunk';
import { MainReducer } from '@src/redux/reducers';

const InitialReducer = combineReducers({ MainReducer });
const store = compose(
  applyMiddleware(thunk)
)(createStore)(InitialReducer);

export default store
