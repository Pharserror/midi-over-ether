import clone from 'lodash/clone';
import get from 'lodash/get';
import merge from 'lodash/merge';
import en from '@src/i18n/en.json';
import INITIAL_STATE from './INITIAL_STATE';

export default function MainReducer(state = INITIAL_STATE, { alerts, data, meta, payload, type }) {
  const newState = clone(state);

  switch (type) {
    case 'ON_AJAX_ERROR': {
      const message = get(en, `http.${payload.status}`, en.http['500']) || '';

      return merge({ newState, alerts: { errors: [message] } }, { data, meta });
    }

    case 'ON_PARSE_DATA_INTO_STORE': {
      return merge({}, newState, { alerts, data, meta });
    }

    default:
      return state;
  }
}
