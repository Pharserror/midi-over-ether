import React from 'react';

export default function HelloWorld(props) {
  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <h1>Hello, world!</h1>
        </div>
      </div>
    </div>
  );
}
