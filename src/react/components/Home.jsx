import faker from 'faker';
import { formHelpers }      from 'swis';
import get                  from 'lodash/get';
import isEqual              from 'lodash/isEqual';
import React, { Component } from 'react';
import { useMetronome }     from 'react-metronome-hook';

import HTTPActions          from '@src/redux/actions/HTTPActions';
import connector            from '@src/redux/connector';
import firebase             from '@src/lib/firebaseConfig';

class Home extends Component {
  constructor(props) {
    super(props);

    // this.chatInput = React.createRef();
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    firebase.database().ref('sessions').on('value', (snapshot) => {
      const sessions = [];

      snapshot.forEach((snap) => { sessions.push(snap.val()); });

      this.props.dispatch(HTTPActions.parseDataIntoStore({ data: { sessions } }));
    });
  }

  shouldComponentUpdate(nextProps) {
    return !isEqual(nextProps.data.sessions, this.props.data.sessions);
  }

  onSubmit(event) {
    event.preventDefault();

    const data = formHelpers.gatherFormData(event);
    // this.chatInput.current.value = '';

    firebase.database().ref('sessions').push({
      content: JSON.stringify(data),
      timestamp: Date.now(),
      uid: faker.random.words(3),
    });
  }

  render() {
    const { sessions = [] } = this.props.data;

    return (
      <div className="col-12" id="trollbox">
        <div className="row">
          <div className="col-12">
            <h5>Home!</h5>
          </div>
        </div>
        <div className="col-12" id="trollbox_chat">
          <div className="row">
            <div className="col-12 sessions">
              <table>
                <thead>
                  <tr>
                    <th>Session ID</th>
                    <th>Session Timestamp</th>
                    <th>BPM</th>
                  </tr>
                </thead>
                <tbody>
                  {sessions.length > 0 && sessions.map((session) => {
                    const content = JSON.parse(session.content);

                    return (
                      <tr key={session.uid}>
                        <td>{session.uid}</td>
                        <td>{session.timestamp}</td>
                        <td>{content.bpm}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className="col-12 pb-4 pt-5">
          <form onSubmit={this.onSubmit}>
            <div className="row">
              <div className="col-6">
                <input className="form-control" name="bpm" type="text" />
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <input
                  className="btn btn-primary form-control"
                  type="submit"
                  value="Send"
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default connector(Home);
