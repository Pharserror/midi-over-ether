import React from 'react';

export default function Loader() {
  return (
    <div className="row">
      <div className="col">
        <h5>Loading ...</h5>
      </div>
    </div>
  );
}
