import { Link } from 'react-router-dom';
import React from 'react';

// NOTE: Potentially refactor this to be a dumb component
export default function SitewideNav(props) {
  return (
    <nav className="bg-dark navbar navbar-light sticky-top" id="site_wide_nav">
      <div className="container">
        <div>
          <Link to="/login">
            Login
          </Link>
        </div>
      </div>
    </nav>
  );
}
