import React from 'react';

export default function Footer(props) {
  return (
    <div className="container" id="footer">
      <div className="row">
        <div className="col-12">
          <div className="row">
            <div className="col-11 col-md-6">
              <h5>Footer</h5>
            </div>
            <div className="col-1 col-md-1 offset-md-5">
              <a href="javascript:window.scrollTo(0,0)">To Top</a>
            </div>
          </div>
          <div className="row" id="copyright">
            <div className="col-3 offset-9 pb-2">
              Your Company Name Here, LLC
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
