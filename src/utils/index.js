import { HTTPThunk } from '@src/utils/http';

export function findOrLoadResources({ dispatch, params = {}, url = '' } = {}) {
  return dispatch(HTTPThunk({
    url,
    data: {
      ...params,
      // page: 1,
      // per_page: 10,
    },
    verb: 'GET',
  }));
}
