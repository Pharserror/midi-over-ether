import axios from 'axios';
import HTTPActions from '@src/redux/actions/HTTPActions';

export function simpleHTTP({
  params = {},
  url = 'localhost',
  verb = 'GET',
  success = () => false,
  error = () => false,
} = {}) {
  return (
    axios[verb](url, { params })
      .then(response => { success(response); })
      .catch(error => { error(error); })
  );
}

export function HTTPThunk(options) {
  return (dispatch) => (
    simpleHTTP({
      ...options,
      success: response => dispatch(HTTPActions.parseDataIntoStore(response)),
      error: error => dispatch(HTTPActions.ajaxError(error)),
    })
  );
}
