require('./main.scss');
import React from 'react';
import { render } from 'react-dom';
import Root from '@src/react/components/Root';
import store from '@src/redux/store';

document.addEventListener('DOMContentLoaded', () => {
  render(
    React.createElement(Root, { store }),
    document.getElementById('root')
  );
});
