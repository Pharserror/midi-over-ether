const path = require('path');
const webpack = require('webpack');
module.exports = {
  context: __dirname,
  entry: {
    app: [
      './client/main.js',
    ],
  },
  mode: 'development',
  module: {
    rules: [{
      test: /\.jsx?$/,
      loader: 'babel-loader',
    }, {
      test: /\.s?css$/,
      loaders: ['style-loader', 'css-loader', 'sass-loader'],
    }],
  },
  output: {
    chunkFilename: '[name].bundle.js',
    path: __dirname + '/public/javascripts',
    filename: '[name].js',
    publicPath: '/',
  },
  plugins: [
    new webpack.EnvironmentPlugin([
      'APP_HYDRATION_URL',
      'FIREBASE_API_KEY',
      'FIREBASE_AUTH_DOMAIN',
      'FIREBASE_DB_URL',
      'FIREBASE_PROJECT_ID',
    ]),
  ],
  resolve: {
    alias: {
      '@lib': path.join(__dirname, 'src/lib'),
      '@src': path.join(__dirname, 'src'),
    },
    extensions: ['.js', '.jsx'],
  },
};
